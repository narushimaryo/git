﻿using UnityEngine;
using System.Collections;

//http://www.gaprot.jp/pickup/unity-mecanim/vol1/
public class ViewerCamera : MonoBehaviour {

	// GameObject(this)
	public GameObject viewObject = null;

	// CONST
	public float rotationSensitivity = 0.01f;
	public float distanceSensitivity = 0.01f;
	public float followObjectSmooth = 3f;
	public float maxRotationY = 0.45f;
	public float minRotationY = -0.45f;
	public float minDistance = 0.5f;
	public float maxDistance = 5f;

	// DEFAULT
	public float defaultDistance = 2f;
	public float defaultAngularPositionX = 0f;
	public float defaultAngularPositionY = 0f;
	
	// Valueable
	protected float distance = 0f;
	protected Vector2 cameraPosParam = Vector2.zero;

	// Parametar before 1 frame
	private Vector3 pivotTemp = Vector3.zero;
	private Vector3 clickedPos = Vector3.zero;
	private Vector2 cameraPosParamTemp = Vector2.zero;
	private float distanceTemp = 0f;

	// Inner Triger
	private int clickedFlag = 0;	// 0:none 1:left 2:right

	// Use this for initialization
	void Start () {

		//  distance
		this.distance = this.defaultDistance;

		// Angular
		this.cameraPosParam = new Vector2 (
			this.defaultAngularPositionX / 180f * Mathf.PI,
			this.defaultAngularPositionY / 180f * Mathf.PI
		);

		// position
		this.pivotTemp = this.transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {

		// Trigger MouseDown
		if(this.clickedFlag == 0){

			// Left Mouse Down
			if(Input.GetMouseButtonDown(0)) {
				this.clickedPos = Input.mousePosition;
				this.cameraPosParamTemp = this.cameraPosParam;
				this.clickedFlag = 1;
			}

			// Right Mouse Down
			if(Input.GetMouseButtonDown(1)) {
				this.clickedPos = Input.mousePosition;
				this.distanceTemp = this.distance;
				this.clickedFlag = 2;
			}
		}

		// Trigger MouseUp
		if (this.clickedFlag == 1 && Input.GetMouseButtonUp(0)) {
			this.clickedFlag = 0;
		}
		if (this.clickedFlag == 2 && Input.GetMouseButtonUp(1)) {
			this.clickedFlag = 0;
		}

		// Difference Distance 
		Vector3 mousePosDistance = Input.mousePosition - this.clickedPos;

		switch(this.clickedFlag) {

		// Transition Camera position
		case 1:
			var diff = new Vector2(mousePosDistance.x, -mousePosDistance.y) * this.rotationSensitivity;
			this.cameraPosParam.x = this.cameraPosParamTemp.x + diff.x;
			this.cameraPosParam.y = Mathf.Clamp(
				this.cameraPosParamTemp.y + diff.y,
				this.minRotationY * Mathf.PI,
				this.maxRotationY * Mathf.PI
			);
			break;
		// Transition to distance
		case 2:
			this.distance = Mathf.Clamp(
				this.distanceTemp + mousePosDistance.y*this.distanceSensitivity,
				this.minDistance,
				this.maxDistance
			);
			break;
		}

		// trans camera
		Vector3 orbitPos = GetOrbitPosition(this.cameraPosParam, this.distance);

		Vector3 pivot = Vector3.Lerp(this.pivotTemp, this.viewObject.transform.position, Time.deltaTime * this.followObjectSmooth);
		this.transform.position = pivot + orbitPos;
		this.transform.LookAt(this.viewObject.transform);

		this.pivotTemp = pivot;
	}
	
	// Trans and Rotate
	private Vector3 GetOrbitPosition(Vector2 anglarParam, float distance) {
		float x = Mathf.Sin (anglarParam.x) * Mathf.Cos (anglarParam.y);
		float y = Mathf.Sin (anglarParam.y);
		float z = Mathf.Cos (anglarParam.x) * Mathf.Cos (anglarParam.y);

		return new Vector3 (x, y, z) * distance;
	}

}
