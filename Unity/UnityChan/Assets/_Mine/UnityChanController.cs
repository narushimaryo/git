﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class UnityChanController : MonoBehaviour {

	private Animator animator;
	private int dowalkId;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();

		// Statementable Boolean
		dowalkId = Animator.StringToHash("DoWalk");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.UpArrow)) {

			// Idle -> Walk
			animator.SetBool(dowalkId, true);

		} else {

			// Walk -> Idle
			animator.SetBool(dowalkId, false);

		}
	}
}