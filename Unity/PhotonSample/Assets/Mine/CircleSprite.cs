﻿using UnityEngine;
using System.Collections;

public class CircleSprite : MonoBehaviour {

	private GameObject KnobObj;


	// Use this for initialization
	void Start () {
		Color randomColor = new Color( Random.value, Random.value, Random.value, 0.75f );
		this.GetComponent<Renderer>().material.color = randomColor;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
