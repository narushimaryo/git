﻿using UnityEngine;

//http://dev.classmethod.jp/etc/unity-photon-2d-physics-online-game-01/
public class NetworkManager : Photon.MonoBehaviour {
	
	private float halfradius = 5f;
	private float randex = 19f;


	void Awake () {
		//マスターサーバーへ接続
		PhotonNetwork.ConnectUsingSettings("v0.1");
	}
	
	void Update () {
	}
	
	//ロビー参加成功時のコールバック
	void OnJoinedLobby() {
		//ランダムにルームへ参加
		PhotonNetwork.JoinRandomRoom();
	}
	
	//ルーム参加失敗時のコールバック
	void OnPhotonRandomJoinFailed() {
		Debug.Log("ルームへの参加に失敗しました");
		//名前のないルームを作成
		PhotonNetwork.CreateRoom(null);
	}
	
	//ルーム参加成功時のコールバック
	void OnJoinedRoom() {
		Debug.Log("ルームへの参加に成功しました");
		
		// instantiate fot player
		Vector3 spawnPosition = new Vector3 (
			Random.value * randex + halfradius,
			Random.value * randex + halfradius,
			0
		);
		PhotonNetwork.Instantiate ("Knob", spawnPosition, Quaternion.identity, 0);
	}
	
	void OnGUI() {
		//サーバーとの接続状態をGUIへ表示
		GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
	}
}